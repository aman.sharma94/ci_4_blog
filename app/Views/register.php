<!DOCTYPE html>
<html>
<head>
	<title>Login | Register | ci-4 blog</title>
</head>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">
<script src="<?php echo base_url('assets/js/jquery-3.2.1.slim.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/popper.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>

<body>
 <div class="container">

 	<div class="row align-items-center mt-5">
 		<div class="container">
 			<div class="row">
 		<div class="col-md-6 mb-0">
 			<div class="font-weight-bold">
 				Register your self for commenting on blogs
 			</div>
 		</div>
 		<div class="col-md-6 mb-0">
 			<div class="card">
 				<div class="card-body">
 					<p class="font-weight-bold">Register Form</p>
 				<form method="post" action="<?php echo base_url('register-save') ?>" class="register" id="register">
 					<div class="form-group">
 						<label for="fullname">Full Name</label>
 						<input class="form-control" id="fullname" required type="text" name="fullname" value="">
 					</div>
 					<div class="form-group">
 						<label for="emailaddress">Email Address</label>
 						<input class="form-control" id="emailaddress" required type="email" name="emailaddress" value="">
 					</div>
 					<div class="form-group">
 						<label for="password">Password</label>
 						<input class="form-control" type="password" required name="password" value="">
 					</div>
 					<div class="form-group">
 						<button class="btn btn-primary" type="submit">Register</button>
 					</div>
 				</form>
 				<div class="row align-items-center">
 					<a class="" href="<?php echo base_url('login'); ?>">Already have account!!</a>
 				</div>
 				</div>
 			</div>
 		</div>
 		</div>
 	</div>
 	</div>
 </div>

</body>
</html>
<!DOCTYPE html>
<html>
<head>
	<title>Login | Register | ci-4 blog</title>
</head>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">
<script src="<?php echo base_url('assets/js/jquery-3.2.1.slim.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/popper.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>

<body>
 <div class="container">

 	<div class="row align-items-center mt-5">
 		<div class="container">
 			<div class="row">
 		<div class="col-md-6 mb-0">
 			<div class="font-weight-bold">
 				Login to your account
 			</div>
 		</div>
 		<div class="col-md-6 mb-0">
 			<div class="card">
 				<div class="card-body">
 					<p class="font-weight-bold">Login Form</p>
 				<form method="post" action="" class="register" id="register">
 					
 					<div class="form-group">
 						<label for="emailaddress">Email Address</label>
 						<input class="form-control" id="emailaddress" type="email" name="emailaddress" value="">
 					</div>
 					<div class="form-group">
 						<label for="password">Password</label>
 						<input class="form-control" type="password" name="password" value="">
 					</div>
 					<div class="form-group">
 						<button class="btn btn-primary" type="submit">Login</button>
 					</div>
 				</form>
 				<div class="row align-items-center">
 					<a class="" href="<?php echo base_url('register'); ?>">New to website!!</a>
 				</div>
 				</div>
 			</div>
 		</div>
 		</div>
 	</div>
 	</div>
 </div>

</body>
</html>